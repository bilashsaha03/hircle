class CreatePriorities < ActiveRecord::Migration
  def change
    create_table :priorities do |t|
      t.string :title
      t.string :color
      t.text :description

      t.timestamps
    end
  end
end
