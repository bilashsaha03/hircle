class CreateSubTasks < ActiveRecord::Migration
  def change
    create_table :sub_tasks do |t|
      t.string :title
      t.text :description
      t.integer :priority_id

      t.timestamps
    end
  end
end
